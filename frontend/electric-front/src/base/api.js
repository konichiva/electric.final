import axios from "axios";

//import Alert from 'react-s-alert';
// import { refreshToken, logout } from './security';
//import { loc } from 'components/alap';

const api = axios.create({
    baseURL: "/api/",
    timeout: 10000,
});


export default api;

api.interceptors.response.use(
    null,
    error => {
        const { response, config } = error;
        if (response) {
            if (response.status === 401 && config && !config.retryRequest && isSecureRequest(config)) {
                
                    (() => {
                        config.url = config.url.substring(api.defaults.baseURL.length); // axios bug!
                        config.retryRequest = true;
                        return api(config);
                    });
            }
            
            // Unauthorized
            if (response.status === 401) {
                if (error.response && error.response.data && error.response.data.result){
                    error.response.data.errorCode = error.response.data.result
                }
               
                return Promise.reject(error.response);
            }
/*
            // Bad Request (BusinessException)
            if (response.status === 400) {
                let errorMsg = loc.msg['error.' + response.data.localizationCode] ? loc.msg['error.' + response.data.localizationCode] : 'error.' + response.data.localizationCode
                if (response.data.attributes) {
                    Object.keys(response.data.attributes).forEach((key) => {
                        errorMsg = errorMsg.replace(':' + key, response.data.attributes[key]);
                    })
                }
                // TODO process attributes
                
                Alert.error('<div class="title">' + errorMsg + '</div>', {
                    position: 'top-right',
                    timeout: 'none'
                });
                
                return Promise.reject(error);
            }

            // Forbidden (ForbiddenException)
            if (response.status === 403) {
                
                Alert.error('<div class="title">' + loc.msg['altalanos.jogosulatlan.hiba'] + '</div>', {
                    position: 'top-right',
                    timeout: 10000
                });
                
                return Promise.reject(error);
            }

            // Internal Server Error (ServiceException)
            if (response.status === 500) {
                
                Alert.error('<div class="title">' + loc.msg['altalanos.varatlan.hiba'] + '</div>' + (response.data.message), {
                    position: 'top',
                    timeout: 'none'
                });
                
                return Promise.reject(error);
            }
            */
        }
        if (!config.suppresTimeoutError) {
         
            /*
            Alert.error('<div class="title">' + loc.msg['altalanos.varatlan.timeout'] + '</div>', {
                position: 'top-right',
                timeout: 'none'
            });
            */
        }
        return Promise.reject(error);
    }
);

api.download = (url, data, config) => {
    return api.post(url, data, { responseType: 'blob', ...config })
        .then(response => {
            const { headers, data } = response;
            const fileName = headers['content-disposition'].match(/filename="(.+)"/)[1];
            const contentType = headers['content-type'];
           
        });
}

function isSecureRequest(config) {
    return config.url.startsWith(api.defaults.baseURL) && !config.skipAuth;
}
