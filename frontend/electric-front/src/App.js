import React, { Component } from 'react';
import './App.css';
import Login from './components/login'
import AdminPanel from './components/adminPanel'
import {Route,withRouter } from 'react-router-dom';
import Home from './components/home'
import Products from './components/products'
import SubMenu from './components/submenu'

class App extends Component {
  render() {
    return (
      <div >
        <Route path="/" component={Home}/>
        
        <Route path="/:menu" component={SubMenu}/>
        <Route path="/admin" component={Login}/>
        <Route path="/adminPanel" component={AdminPanel}/>
        <Route path="/products" component={Products}/>
      </div>
    );
  }
}

export default App;
