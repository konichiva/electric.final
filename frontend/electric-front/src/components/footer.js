import React from 'react';
import {Col} from 'react-bootstrap';
import {Row} from 'react-bootstrap';
import {Navbar} from 'react-bootstrap';
import {Nav} from 'react-bootstrap';
import {NavItem} from 'react-bootstrap';
import SubMenu from './submenu';




export default class Footer extends React.Component {

    render() {
      return (
        <div > 
            <Row>
            <Navbar>
                <Row>
                    <Col md={4}>
                        Miercurea Ciuc B-dul Timișoarei Nr 24 
                    </Col>
                    <Col md={4}>
                        Tel/FAX: 0366-401003 
                    </Col>
                    <Col md={4}>
                            E-mail: office@electric.com.ro
                    </Col>
                </Row>
                </Navbar>
            </Row>
      
        </div>
      );
    }
  }