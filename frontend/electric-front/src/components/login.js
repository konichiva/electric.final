import React from 'react';
import {Panel} from 'react-bootstrap';
import {Button} from 'react-bootstrap';
import {FormGroup} from 'react-bootstrap';
import {ControlLabel} from 'react-bootstrap';
import {FormControl} from 'react-bootstrap';
import {Col} from 'react-bootstrap';
import {Row} from 'react-bootstrap';
import api from '../base/api';

export default class Login extends React.Component {
    constructor(props, context) {
      super(props, context);
      this.userNameHandleChange = this.userNameHandleChange.bind(this);
      this.passwordHandleChange = this.passwordHandleChange.bind(this);
      this.state = {
        userName:"",
        password:"",
        showAlert:false
      };
    }
    userNameHandleChange(e) {
      this.setState({ userName: e.target.value });
    }
    passwordHandleChange(e) {
        this.setState({ password: e.target.value });
      }

    login=()=>{
    const  admin={
            id:null,
            userName:this.state.userName,
            password:this.state.password
        }   
        api.post("user/login", admin).then(response=>{
            if(response.data){
                if(response.data[0].id){
                    this.props.history.push('/adminPanel')
                }else{
                    window.alert("login falied")
                }
            }else{
                window.alert("login falied")
            }
        })
    }
    render() {
        
      return (
    <div>
        <Panel>
            <Panel.Heading >
                 <Panel.Title componentClass="h3">Login</Panel.Title>
            </Panel.Heading>
            <Panel.Body>
                <FormGroup  >
                    <Row>
                        <Col md ={3}>
                        </Col>
                        <Col md={6}>
                            <ControlLabel>User name</ControlLabel>
                            <   FormControl type="text"  id={"userName"}  value={this.state.userName} placeholder="Enter text"  onChange={this.userNameHandleChange} />
                            <FormControl.Feedback />
                        </Col>
                    </Row>
                    <Row>
                        <Col md ={3}>
                        </Col>
                        <Col md={6}>
                            <ControlLabel>Password</ControlLabel>
                                <FormControl type="password" id={"password"} value={this.state.password} placeholder="Enter password"   onChange={this.passwordHandleChange}/>
                            <FormControl.Feedback />
                        </Col>
                    </Row>
                    <Row>
                        <Col md ={3}>
                        </Col>
                        <Col md={6}>
                            <Button onClick={(event)=>{this.login()}} bsStyle="primary">Login</Button>
                        </Col>
                    </Row>
                </FormGroup>  
        </Panel.Body>
    </Panel>
    </div>
      );
    }
  }