import React from 'react';
import {Col} from 'react-bootstrap';
import {Row} from 'react-bootstrap';
import {Navbar} from 'react-bootstrap';
import {Nav} from 'react-bootstrap';
import {NavItem} from 'react-bootstrap';
import SubMenu from './submenu';
import '../App.css';
import api from '../base/api';
// import {NavDropdown} from 'react-bootstrap';


export default class NavBar extends React.Component {
  constructor(props, context) {
    
    super(props, context)
    this.slideMenu = React.createRef();
    this.state = {
       menu:"",
       menuOpen:false,
       openSlide:true,
       subMenu:"",
       sidebarOpen: false,
       };
       this.onSetSidebarOpen = this.onSetSidebarOpen.bind(this);
    
  }
  
    componentDidMount(){
      this.getNavElement();
      this.onSetSidebarOpen();
    
    }
    onSetSidebarOpen=()=> {
      this.setState({sidebarOpen: true ,openSlide:true});
      // console.log(this.state.sidebarOpen)
    }


    getNavElement=()=>{
      let menu = [];
      api.get("termek/menu").then(response=>{
        console.log(response)
          if(response){
            response.data.map(element =>{
              if(!menu.includes(element)){
                menu.push(element)
              }
              this.setState({menu: menu})
            
            })
          }
               
      })
      
     
    }

    getSlideMenuElements=(menuElement)=>{
      let subMenu = [];
      // this.props.history.push('/'+menuElement.replace(/ /g,"_"))
      this.props.history.push({
        pathname: '/',
        search: menuElement.replace(/ /g,"_"),
      })

      api.get("termek/almenu",{params:{menu:menuElement}} ).then(response=>{
       if(response.data){
        response.data.map(element =>{
          if(!subMenu.includes(element)){
            subMenu.push(element)
          }
          this.setState({subMenuElement: subMenu})
        })
       }
                   
      })
    }
    
    render() {
  
      if(!this.state.menu) return null
      return (
        <div>
        <Col md={12}>
        <Row>
          <Navbar>
            <Row>
              <Col md={3}>
                <Navbar.Header>
                    <Navbar.Brand>
                    <a href="#home">Magazinul Instalațiilor Electrice</a>
                    </Navbar.Brand>
                </Navbar.Header>
              </Col>
              </Row>
              <Row>

                <Nav>
                    {this.state.menu.map((element, index)=>
                      <NavItem key={index} onClick={(event)=>{this.getSlideMenuElements(element)}}eventKey={index} href="#"> {element}</NavItem>
                    )}
                </Nav>
              </Row>
            </Navbar>
        </Row>
        </Col>
     </div>
      );
    }
  }