import React from 'react';
// import {Panel} from 'react-bootstrap';
// import {Button} from 'react-bootstrap';
// import {FormGroup} from 'react-bootstrap';
// import {ControlLabel} from 'react-bootstrap';
// import {FormControl} from 'react-bootstrap';
// import {Col} from 'react-bootstrap';
// import {Row} from 'react-bootstrap';
// import Dropzone from 'react-dropzone'
import NavBar from './navBar'
import Footer from './footer'
import SubMenu from './submenu'


export default class Home extends React.Component {
    constructor(props, context) {
      super(props, context)
      this.child = React.createRef();
      this.state = { child: null };
    }
   
    
    componentDidMount() {
    }


    render() {
  
      return (
      <div id="outer-container">
          <NavBar  history={this.props.history}  getProduct={this.state.child}pageWrapId={"page-wrap"} outerContainerId={"outer-container"}/>
          <SubMenu  history={this.props.history} />
         <div style={{ position: 'fixed', bottom: 0,left: 0, right: 0}}>
          <Footer/>
        </div>
    </div>
      );
    }
  }