import React from 'react';
import {Panel} from 'react-bootstrap';
import {Button} from 'react-bootstrap';
import Dropzone from 'react-dropzone'
import api from '../base/api';

export default class ExcelUpdate extends React.Component {
    constructor(props, context) {
      super(props, context);
      this.onDrop =   this.onDrop.bind(this);
      this.upload = this.upload.bind(this);
      this.state = { files: "" };
    }
   
    onDrop(acceptedFiles, rejectedFiles, e) {
      this.setState({
          files: acceptedFiles,
          filesWereDropped: !e.target.files || e.target.files.length === 0
      });
  }

  upload(e) {
    e.preventDefault();
    let formData = new FormData(this.formRef);
    if(this.state.files.length>0){
      this.state.files.forEach(file => {
          formData.append('inputfile', file, file.name);
      });
      api.post('termek/uploadtermek',formData).then(response=>{
        this.setState({files:""})
      })
    }else{
      window.alert("Empty upload")
    }
 
}

    render() {
      return (
    <div>
       <Panel>
            <Panel.Heading >
                 <Panel.Title componentClass="h3">Update Excel</Panel.Title>
            </Panel.Heading>
            <Panel.Body>
              
              <div style={{"margin": "auto", "width": "200px", "height": "200px"}}className="dropzone">
                <Dropzone    accept=".xls,.xlsx" onDrop={this.onDrop}>
                  <p>Try dropping some files here, or click to select files to upload.</p>
                </Dropzone>
                
              </div>
              {this.state.files?
              <div>
                <h2>Dropped files</h2>
                <ul>
                    {this.state.files.map(f => <li key={f.name}>{f.name} - {f.size} bytes</li>)}
                </ul>
                <Button onClick={this.upload} bsStyle="primary">Upload</Button>
              </div>:null}
        </Panel.Body>
    </Panel>
    </div>
      );
    }
  }