import { slide as Menu } from 'react-burger-menu'
import {NavItem} from 'react-bootstrap';
import React, {Component} from 'react';
import '../App.css';

export default class SlideBar extends Component {
    constructor(props, context) {
        super(props, context)
        this.getTermek - this.getTermek.bind(this)
        this.state = { alMenu: null };
        
      }
      

    getTermek=(event)=>{
        this.props.history.push({
            pathname: '/products',
            search: event
          })
    }

    render() {
        if(!this.props.subMenuElement)return ( <Menu isOpen={this.props.menuOpen}   noOverlay  stack>
            <div>Empty</div>
                </Menu>)
        return (
           <div>
                <Menu pageWrapId={this.props.pageWrapId} outerContainerId={this.props.outerContainerId }   isOpen={this.props.menuOpen}  noOverlay > 
            {this.props.subMenuElement.map((element, index)=> <NavItem key={index} onClick={(event)=>{this.getTermek(element)}}eventKey={index} href="#"> {element}</NavItem>)}
                </Menu>
            </div>
        );
    }
}
