package model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="termek")
public class Termek {
	
	@Id
	@SequenceGenerator(name="termek_sequence",sequenceName="termek_id_seq", allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator="termek_sequence")
	private Long id;
	
	@Column(name="termek_nev")
	private String ternekNev;

	@Column(name="menu")
	private String menu;
	
	@Column(name="ar")
	private BigDecimal ar;
	
	@Column(name="mertekegyseg")
	private String mertekegyseg;
	
	@Column(name="al_menu")
	private String alMenu;

	@Column(name="termek_leiras", length=1500)
	private String termekLeiras;
	
	@Column(name="kep_utvonal")
	private String kepUtvonal;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTernekNev() {
		return ternekNev;
	}

	public void setTernekNev(String ternekNev) {
		this.ternekNev = ternekNev;
	}

	public String getMenu() {
		return menu;
	}
	
	public void setMenu(String menu) {
		this.menu = menu;
	}

	public BigDecimal getAr() {
		return ar;
	}

	public void setAr(BigDecimal ar) {
		this.ar = ar;
	}

	public String getMertekegyseg() {
		return mertekegyseg;
	}

	public void setMertekegyseg(String mertekegyseg) {
		this.mertekegyseg = mertekegyseg;
	}

	public String getAlMenu() {
		return alMenu;
	}

	public void setAlMenu(String alMenu) {
		this.alMenu = alMenu;
	}

	public String getTermekLeiras() {
		return termekLeiras;
	}

	public void setTermekLeiras(String termekLeiras) {
		this.termekLeiras = termekLeiras;
	}

	public String getKepUtvonal() {
		return kepUtvonal;
	}

	public void setKepUtvonal(String kepUtvonal) {
		this.kepUtvonal = kepUtvonal;
	}
	
	

}
