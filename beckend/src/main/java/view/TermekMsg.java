package view;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class TermekMsg {
	
	private Long id;
	private String ternekNev;
	private String menu;
	private BigDecimal ar;
	private String alMenu;
	private String termekLeiras;
	private String mertekEgyseg;
	private String kepUtvonal;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTernekNev() {
		return ternekNev;
	}
	public void setTernekNev(String ternekNev) {
		this.ternekNev = ternekNev;
	}
	public String getMenu() {
		return menu;
	}
	public String getMertekEgyseg() {
		return mertekEgyseg;
	}
	public void setMertekEgyseg(String mertekEgyseg) {
		this.mertekEgyseg = mertekEgyseg;
	}
	public void setMenu(String menu) {
		this.menu = menu;
	}
	public BigDecimal getAr() {
		return ar;
	}
	public void setAr(BigDecimal ar) {
		this.ar = ar;
	}
	public String getAlMenu() {
		return alMenu;
	}
	public void setAlMenu(String alMenu) {
		this.alMenu = alMenu;
	}
	public String getTermekLeiras() {
		return termekLeiras;
	}
	public void setTermekLeiras(String termekLeiras) {
		this.termekLeiras = termekLeiras;
	}
	public String getKepUtvonal() {
		return kepUtvonal;
	}
	public void setKepUtvonal(String kepUtvonal) {
		this.kepUtvonal = kepUtvonal;
	}
}
