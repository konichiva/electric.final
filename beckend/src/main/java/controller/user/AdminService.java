package controller.user;

import java.util.List;

import javax.ejb.Local;

import model.Admin;
import view.AdminMsg;
@Local
public interface AdminService {

	public Admin getById (Long id);
	public List<Admin> login(String userName, String password);
	
}
