package controller.user;



import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Admin;
import view.AdminMsg;
@Stateless(name="AdminService")
public class AdminServiceBean implements AdminService{

	@PersistenceContext
	EntityManager em;
	
	@Inject
	AdminConverter adminConverter;

	public Admin getById(Long id) {
		return em.createQuery("from User where id=:id", Admin.class).setParameter("id", id).getSingleResult();
	}

	public List<Admin> login(String userName, String password) {
	
		List<Admin> admins=em.createQuery("select a from "+Admin.class.getName()+" a where a.userName=:userName AND a.password=:password", Admin.class)
				.setParameter("userName", userName)
				.setParameter("password", password)
				.getResultList();
		if(admins.size()>0) {
			return admins;
		}
		return null;
	}

}
