package controller.user;

import javax.ejb.Stateless;

import model.Admin;
import view.AdminMsg;
@Stateless(name="UserConverter")
public class AdminConverter {
	
	public AdminMsg toMsg(Admin from){
		AdminMsg to =  new AdminMsg();
		to.setId(from.getId());
		to.setPassword(from.getPassword());
		to.setUserName(from.getUserName());
		return to;		
	}
	
	public Admin toEntity(AdminMsg from) {
		Admin to = new Admin();
		to.setId(from.getId());
		to.setPassword(from.getPassword());
		to.setUserName(from.getUserName());
		
		return to;
	}
}
