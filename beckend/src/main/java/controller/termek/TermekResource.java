package controller.termek;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInputImpl;

import controller.user.AdminService;
import view.TermekMsg;


@Path("termek")
public class TermekResource {
	
	@Inject
	private TermekService termekService;
	
	
	
	@POST
	@Path("/uploadtermek")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public void insertDataInDB2(MultipartFormDataInput  inputfile) {
		termekService.uploadExcel(inputfile);
	}
	
	@GET
	@Path("/menu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<String> getMenu() {
		return termekService.getMenu();
	}
	
	@GET
	@Path("/almenu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<String> getAlMenu(@QueryParam("menu") String menu ) {
		System.out.println(menu);
		return termekService.getAlMenu(menu);
	}
	
	@GET
	@Path("/gettermekbyalmenu")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public List<TermekMsg> getTermekByAlmenu(@QueryParam("almenu") String almenu ) {
		System.out.println(almenu);
		return termekService.getTermekByAlmenu(almenu);
	}
}
