package controller.termek;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import model.Termek;
import view.TermekMsg;
@Stateless(name="TermekService")
public class TermekServiceBean implements TermekService{
	
	@PersistenceContext
	EntityManager em;

	@Inject
	TermekConverter termekConverter;

	
	public void uploadExcel(MultipartFormDataInput inputfile ) {
		for(int i=0; i< getValuesFromExcel(inputfile, "Termek neve" ).size()-1;i++) {
			Termek termek =  new Termek();
			
			
			termek.setTernekNev(getValuesFromExcel(inputfile, "Termek neve" ).get(i));
			termek.setAlMenu(getValuesFromExcel(inputfile, "Al Menu" ).get(i));
			termek.setTermekLeiras(getValuesFromExcel(inputfile, "Termek leiras" ).get(i));
			termek.setMertekegyseg(getValuesFromExcel(inputfile, "Mertekegyseg" ).get(i));
			termek.setMenu(getValuesFromExcel(inputfile, "Menu" ).get(i));
			termek.setKepUtvonal(getValuesFromExcel(inputfile, "Kep Utvonal" ).get(i));
			if(!getValuesFromExcel(inputfile, "Ar" ).get(i).isEmpty()) {
				termek.setAr(new BigDecimal(getValuesFromExcel(inputfile, "Ar" ).get(i).replace(',', '.')));
			}
			
			saveTermek(termek);
		}
		System.out.println(getValuesFromExcel(inputfile, "Termek neve" ));
		System.out.println(getValuesFromExcel(inputfile, "Menu" ));
		System.out.println(getValuesFromExcel(inputfile, "Ar" ));
		System.out.println(getValuesFromExcel(inputfile, "Al Menu" ));
		System.out.println(getValuesFromExcel(inputfile, "Termek leiras" ));
		System.out.println(getValuesFromExcel(inputfile, "Mertekegyseg" ));
		System.out.println(getValuesFromExcel(inputfile, "Kep Utvonal" ));
	}

	public List <String> getValuesFromExcel(MultipartFormDataInput inputfile,  String columnName ) {
		List <String> ertekek = new ArrayList<String>();
		Map<String, List<InputPart>> uploadForm = inputfile.getFormDataMap();
	    List<InputPart> inputParts = uploadForm.get("inputfile");
	    String CellValue= "";
	    InputPart inputPart = inputParts.get(0);
	    try {
	    	String SheetName = "Termekek";
	        InputStream inputStream = inputPart.getBody(InputStream.class, null);

	        HSSFWorkbook workbook = new HSSFWorkbook(inputStream);
	        Sheet sheet =  workbook.getSheet(SheetName);
	        Row row = sheet.getRow(0);
	        short lastcolumnused = row.getLastCellNum();
	        int colnum = 0;
	       for (int i = 0; i < lastcolumnused; i++) {
	        if (row.getCell(i).getStringCellValue().equalsIgnoreCase(columnName)) {
	        	colnum = i;
	            break;
	        }
	       }
	       for(int j=1; j<=sheet.getLastRowNum(); j++) {
	    	   DataFormatter formatter = new DataFormatter();
	    	   row = sheet.getRow(j);
		       CellValue = formatter.formatCellValue(row.getCell(colnum));
		       ertekek.add(CellValue);
	       }
	      
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	   
		return ertekek;
	}

	public void saveTermek(Termek termek) {
		em.merge(termek);
	}
	
	public List<String> getMenu(){
		List<String> menuElements = em.createQuery("Select t.menu from Termek t", String.class).getResultList();
		return  menuElements;
	}
	
	public List<String> getAlMenu(String menu){
		List<String> menuElements = em.createQuery("Select t.alMenu from Termek t Where t.menu=:menu ", String.class).setParameter("menu", menu).getResultList();
		return  menuElements;
	}

	public List<TermekMsg> getTermekByAlmenu(String almenu) {
		List<Termek> termekek = em.createQuery("from Termek t Where t.alMenu=:al_menu ", Termek.class).setParameter("al_menu", almenu).getResultList();	
		List<TermekMsg> termekMsgList = new ArrayList<TermekMsg>();
		if(termekek.size()>0) {
			for (Termek termek : termekek) {
				termekMsgList.add(termekConverter.toMsg(termek));
			}
		}
		return termekMsgList;
	}	
}
