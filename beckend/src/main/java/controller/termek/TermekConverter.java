package controller.termek;

import javax.ejb.Stateless;

import model.Termek;
import view.TermekMsg;
@Stateless(name="TermekConverter")
public class TermekConverter {
	
	public Termek toEntity(TermekMsg from) {
		Termek to= new Termek();
			to.setId(from.getId());
			to.setAlMenu(from.getAlMenu());
			to.setAr(from.getAr());
			to.setMertekegyseg(from.getMertekEgyseg());
			to.setTermekLeiras(from.getTermekLeiras());
			to.setKepUtvonal(from.getKepUtvonal());
			to.setMenu(from.getMenu());
			to.setTernekNev(from.getTernekNev());
	return to;
	}
	
	public TermekMsg toMsg(Termek from) {
		TermekMsg to = new TermekMsg();
		to.setId(from.getId());
		to.setAlMenu(from.getAlMenu());
		to.setMertekEgyseg(from.getMertekegyseg());
		to.setAr(from.getAr());
		to.setTermekLeiras(from.getTermekLeiras());
		to.setKepUtvonal(from.getKepUtvonal());
		to.setMenu(from.getMenu());
		to.setTernekNev(from.getTernekNev());
	return to;	
	}

}
