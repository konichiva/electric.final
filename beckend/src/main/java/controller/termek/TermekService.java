package controller.termek;

import java.util.List;
import javax.ejb.Local;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import model.Termek;
import view.TermekMsg;

@Local
public interface TermekService {

	public List <String> getValuesFromExcel(MultipartFormDataInput inputfile,  String columnName );
	public void uploadExcel(MultipartFormDataInput inputfile );
	public void saveTermek(Termek termek);
	public List<String> getMenu();
	public List<String> getAlMenu(String menu);
	public List<TermekMsg> getTermekByAlmenu(String almenu);
}
